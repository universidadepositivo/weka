package weka.br.com.michelcandido;

import java.io.FileReader;

import weka.clusterers.SimpleKMeans;
import weka.core.Instances;

public class ManualKmens extends SimpleKMeans {

	/**
	 * Caminho do arquivo ARFF
	 */
	public static String FILE_ARF = "C:\\Program Files (x86)\\Weka-3-7\\data\\weather.nominal.arff";

	public static void main(String[] args) {
		try {
			// Read the instances from a file.
			FileReader reader = new FileReader(ManualKmens.FILE_ARF);
			Instances instances = new Instances(reader);
			// We want to ignore the class of the data vectors.
			instances.deleteAttributeAt(2);
			// Create the clusterer.
			ManualKmens thisClusterer = new ManualKmens();
			thisClusterer.setNumClusters(4);
			thisClusterer.buildClusterer(instances);
			System.out.println(thisClusterer.getSquaredError());
			// Try again with a different number of clusters.
			thisClusterer.setNumClusters(5);
			thisClusterer.buildClusterer(instances);
			System.out.println(thisClusterer.getSquaredError());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	} // end main
} // end class
