package weka.br.com.michelcandido;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

/**
 * 
 * @author Michel Candido
 *
 */
public class LerArff {

	/**
	 * Caminho do arquivo ARFF
	 */
	public static String FILE_ARF = "C:\\Program Files (x86)\\Weka-3-7\\data\\iris.arff";
	
	public static void main(String[] args) throws IOException {
		FileReader reader = new FileReader(LerArff.FILE_ARF);
		
		/**
		 * Cria instancia do WEKA
		 */
		Instances instances = new Instances(reader);
		
		System.out.println("Relation Name: "+instances.relationName());

		System.out.println("Num Attributes: "+instances.numAttributes());
				
		for(int i=0;i<instances.numAttributes();i++) {
			String name = instances.attribute(i).name();
			int type = instances.attribute(i).type();
			String typeName = "";
			switch(type) {
				case Attribute.NUMERIC: typeName = "Numeric"; break;
				case Attribute.NOMINAL: typeName = "Nominal"; break;
				case Attribute.STRING: typeName = "String"; break;
			}
			System.out.println(name+" type "+typeName);
		}
		// Show the data.
		for(int i=0;i<instances.numInstances();i++) {
			Instance instance = instances.instance(i); // ugh
			System.out.println((i+1)+": "+instance+ " missing? "+instance.hasMissingValue());
		}
	}

}
